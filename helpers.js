/**
*
* @return boolean
*
*/
module.exports = {
    isRepeated: function(usedQuotes, authorIndex, quoteIndex)
    {
	return usedQuotes.some((quote) => {
	    return quote[0] === authorIndex && quote[1] === quoteIndex
	})
    },

    usedAllQuotes: function(allQuotes, usedQuotes)
    {
	var all = 0
	allQuotes.forEach((quote) => {
	    all += quote.quotes.length
	})

	return usedQuotes.length === all
    }
}
