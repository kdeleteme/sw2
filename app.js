var express = require('express')
var cors = require('cors')
var app = express()

var port = process.env.PORT || 8080
var quotes = require('./quotes')

var helpers = require('./helpers')
var isRepeated = helpers.isRepeated
var usedAllQuotes = helpers.usedAllQuotes

var usedQuotes = []

app.use(cors())
app.get('/', (req, res) => {
    res.send('Hello')
})

app.get('/api', (req, res) => {
    res.jsonp(quotes)
})

app.get('/api/random', (req, res) => {
    var authorIndex = Math.floor(Math.random() * quotes.sw.length)
    var quoteIndex = Math.floor(Math.random() * quotes.sw[authorIndex].quotes.length)

    while(isRepeated(usedQuotes, authorIndex, quoteIndex))
    {
	authorIndex = Math.floor(Math.random() * quotes.sw.length)
	quoteIndex = Math.floor(Math.random() * quotes.sw[authorIndex].quotes.length)
    }

    usedQuotes.push([authorIndex, quoteIndex])

    if (usedAllQuotes(quotes.sw, usedQuotes))
    {
	usedQuotes = []
    }
    res.set('Content-Type', 'application/json')
	.jsonp({
	    "author": quotes.sw[authorIndex].author,
	    "quote": quotes.sw[authorIndex].quotes[quoteIndex]
	})
})

app.listen(port, () => {
    console.log('App listening on port', port)
})
